import { expect } from "chai";
var chai = require('chai');
chai.use(require('chai-json-schema'));
// const schemas = require('../api/specs/data/schemas_testData.json');

export function statusCodeVerification(res, statusCode: 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500) {
     expect(res.statusCode, `Status Code should be 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500`).to.be.equal(statusCode);
};

export function statusCodeErrorVerification(res) {
     expect(res.statusCode, `Status Code shouldn't be 200`).to.not.equal(200);
     expect(res.body.message, `The user with invalid credentials shouldn't be registered`).to.not.include("is already registered.");
};

export function statusCode200(res) {
     expect(res.statusCode, `Status Code should be 200`).to.be.equal(200);
};

export function respTimeVerification(res, time) {
     expect(res.timings.phases.total, `Response time should be less than 500ms`).to.be.lessThan(time);
};

export function schemaVerification(res, schema) {
     expect(res.body).to.be.jsonSchema(schema);
};

export async function  getRandomArticleId(login) {
     function randomInteger(min: number, max: number) {
          let rand = min + Math.random() * (max + 1 - min);
          return Math.floor(rand);
     };
     let allArticles = (await login.article.getListOfArticles()).body;
     return allArticles[randomInteger(0, allArticles.length - 1)].id;
}