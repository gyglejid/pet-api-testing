import { ApiRequest } from "../request";
import { BaseController } from "./base.controller";
import {operations} from "../../../../.temp/types"
let baseUrl = global.appConfig.baseUrl;

export class LoginUser extends BaseController{
     async getBearerToken(credentials: { email: string; password: string }) {
          
          return (await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("POST")
               .body(credentials)
               .url(`auth/login`)
               .send<operations['authenticateUserUsingPOST']['responses']['200']['schema']>()).body.accessToken;
     }
}
