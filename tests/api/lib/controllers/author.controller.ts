import { ApiRequest } from "../request";
import { BaseController } from "./base.controller";
import {operations} from "../../../../.temp/types"
let baseUrl = global.appConfig.baseUrl;

export class AuthorController extends BaseController {
     async getAuthorInfo() {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("GET")
               .url(`author`)
               .headers({ Authorization: `Bearer ${this.token}`} )
               .send<operations['getSettingsUsingGET']['responses']['200']['schema']>();
          return response;
     }

     async changeAuthorInfo(autorInfoNew:{
                    avatar?: string | undefined,
                    biography?: string | undefined,
                    company?: string | undefined,
                    firstName?: string | undefined,
                    id?: string | undefined,
                    job?: string | undefined,
                    lastName?: string | undefined,
                    location?: string | undefined,
                    twitter?: string | undefined,
                    userId?: string | undefined,
                    website?: string | undefined
                    }) {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("POST")
               .url("author")
               .headers({ Authorization: `Bearer ${this.token}`} )
               .body(autorInfoNew)
               .send<operations['setSettingsUsingPOST']['responses']['200']['schema']>();
          return response;
     }

      async getAuthorOverview(authorId: string | undefined) {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("GET")
               .url(`author/overview/${authorId}`)
               .headers({ Authorization: `Bearer ${this.token}`} )
               .send();
          return response;
     }
}