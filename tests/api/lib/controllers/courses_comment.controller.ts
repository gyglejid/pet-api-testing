import { ApiRequest } from "../request";
import { BaseController } from "./base.controller";
import {operations} from "../../../../.temp/types"
let baseUrl = global.appConfig.baseUrl;
export class CoursesCommentController extends BaseController {
     async getCourseCommentById(courseId: string, opt: any) {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("GET")
               .url(`course_comment/of/${courseId}`)
               .searchParams(new URLSearchParams(opt))
               .headers({ Authorization: `Bearer ${this.token}`} )
               .send<operations['getCommentsByCourseUsingGET']['responses']['200']['schema']>();
          return response;
     }

     async postCommentOnCourse(comment: {
          courseId: string | undefined,
          // id: string,
          text: string | undefined
     }) {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("POST")
               .url(`course_comment`)
               .body(comment)
               .headers({ Authorization: `Bearer ${this.token}`} )
               .send<operations['saveCommentUsingPOST_1']['responses']['200']['schema']>();
          return response;
     }
}

