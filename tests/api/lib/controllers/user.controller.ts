import { ApiRequest } from "../request";
import { BaseController } from "./base.controller";
import {operations} from "../../../../.temp/types"
let baseUrl = global.appConfig.baseUrl;

export class UserController extends BaseController {
     async getUserInfo() {
          return await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("GET")
               .headers({ Authorization: `Bearer ${this.token}`} )
               .url(`user/me`)
               .send<operations['getCurrentUserUsingGET']['responses']['200']['schema']>()
     }
}