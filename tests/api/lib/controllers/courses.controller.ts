import { ApiRequest } from "../request";
import { BaseController } from "./base.controller";
import {operations} from "../../../../.temp/types"
let baseUrl = global.appConfig.baseUrl;
export class CoursesController extends BaseController {
     async getAllCourses() {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("GET")
               .url(`course/all`)
               .headers({ Authorization: `Bearer ${this.token}`} )
               .send<operations['getAllCoursesUsingGET']['responses']['200']['schema']>();
          return response;
     }

}

