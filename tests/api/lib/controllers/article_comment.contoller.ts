import { ApiRequest } from "../request";
import { BaseController } from "./base.controller";
import {operations} from "../../../../.temp/types"
let baseUrl = global.appConfig.baseUrl;
export class ArticleCommentController extends BaseController {
   
     async postCommentOnArticle(comment: {
          articleId: string | undefined,
          // id: string,
          text: string | undefined
     }) {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl)
               .method("POST")
               .url("article_comment")
               .headers({ Authorization: `Bearer ${this.token}` })
               .body(comment)
               .send<operations['saveCommentUsingPOST']['responses']['200']['schema']>();
          return response;
     }

     async getAllCommentsOnArticle(articleId: string | undefined, opt: any) {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl)
               .method("GET")
               .url(`article_comment/of/${articleId}`)
               .searchParams(new URLSearchParams(opt))
               .headers({ Authorization: `Bearer ${this.token}` })
               .send<operations['getCommentsByArticleUsingGET']['responses']['200']['schema']>();
          return response;
     }

}