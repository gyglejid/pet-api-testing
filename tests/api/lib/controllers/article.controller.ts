import { ApiRequest } from "../request";
import { BaseController } from "./base.controller";
import {operations} from "../../../../.temp/types"
let baseUrl = global.appConfig.baseUrl;

export class ArticleController extends BaseController {
   
     async postArticle(article: {
                    authorId: string,
                    authorName: string,
                    id: string,     
                    image: string,
                    name: string,
                    text: string,
     }) {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("POST")
               .url("article")
               .headers({ Authorization: `Bearer ${this.token}` })
               .body(article)
               .send<operations['saveArticleUsingPOST']['responses']['200']['schema']>();
          return response;
     }

     async getListOfArticles() {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("GET")
               .url("article/author")
               .headers({ Authorization: `Bearer ${this.token}` })
               .send<operations['getArticlesUsingGET']['responses']['200']['schema']>();
          return response;
     }

      async getArticleById(articleId: string | undefined) {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("GET")
               .url(`article/${articleId}`)
               .headers({ Authorization: `Bearer ${this.token}` })
               .send<operations['getArticleUsingGET']['responses']['200']['schema']>();
          return response;
     }
     
}