import { ApiRequest } from "../request";
import { BaseController } from "./base.controller";
import {operations} from "../../../../.temp/types"
let baseUrl = global.appConfig.baseUrl;

export class StudentController extends BaseController {
     async getStudentInfo() {
          const response = await new ApiRequest()
               .prefixUrl(baseUrl)
               .method("GET")
               .url(`student`)
               .headers({ Authorization: `Bearer ${this.token}` })
               .send<operations['getSettingsUsingGET_1']['responses']['200']['schema']>();
          return response;
     }

}