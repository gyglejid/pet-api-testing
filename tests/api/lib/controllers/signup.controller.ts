import { ApiRequest } from "../request";
import {operations} from "../../../../.temp/types"
let baseUrl = global.appConfig.baseUrl;

export class Register {
     async signUp(credentials: { email: string; password: string } | { email: undefined; password: string }) {
          return (await new ApiRequest()
               .prefixUrl(baseUrl )
               .method("POST")
               .body(credentials)
               .url(`auth/signup`)
               .send<operations['registerUserUsingPOST']['responses']['200']['schema']>()
          );
     }
}
