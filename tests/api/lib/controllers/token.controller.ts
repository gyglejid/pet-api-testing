import { ArticleController } from "./article.controller";
import { ArticleCommentController } from "./article_comment.contoller";
import { LoginUser } from "./auth.controller";
import { AuthorController } from "./author.controller";
import { CoursesController } from "./courses.controller";
import { CoursesCommentController } from "./courses_comment.controller";
import { StudentController } from "./student.controller";
import { UserController } from "./user.controller";

export class AddToken {
     public readonly commentOnArt: ArticleCommentController;
     public readonly article: ArticleController;
     public readonly author: AuthorController;
     public readonly user: UserController;
     public readonly auth: LoginUser;
     public readonly courses: CoursesController;
     public readonly commentOnCours: CoursesCommentController;
     public readonly student: StudentController;


     constructor(token?) {

          this.commentOnArt = new ArticleCommentController(token);
          this.article = new ArticleController(token);
          this.author = new AuthorController(token);
          this.user = new UserController(token);
          this.auth = new LoginUser(token);
          this.courses = new CoursesController(token);
          this.commentOnCours = new CoursesCommentController(token);
          this.student = new StudentController(token);

     }
     static async login(credentials: { email: string; password: string }) {
          return new AddToken(await new AddToken().auth.getBearerToken(credentials))
     }    

}

