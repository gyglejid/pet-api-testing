import { expect } from 'chai';
import { AddToken } from '../lib/controllers/token.controller';
import { afterEach } from "mocha";

import { respTimeVerification, statusCode200, statusCodeErrorVerification, statusCodeVerification } from '../../helpers/functionForTests.helper';
import { Register } from '../lib/controllers/signup.controller';


let credentials = global.appConfig.users.studentDude1;
     let invalidCredentialsDataSet = [
               { email: "тріми-вали", password: "1234a-AA" },
               { email: ".afs.@ds.es", password: "1234a-AA" },
               { email: ".", password: "1234a-AA" },
               { email: "@dfj.es", password: "1234a-AA" },
               { email: "тцра@.", password: "1234a-AA" },
               { email: "aw@", password: "1234a-AA" },
               { email: "awe@-ddfe-.es", password: "1234a-AA" },
               { email: "awe@w.e", password: "1234a-AA" },
               { email: "awe@]]e", password: "1234a-AA" },
               { email: "awe@її,?e", password: "1234a-AA" },
               { email: "adwe@i.ua", password: " " },
               { email: "auw@i.ua", password: "111" },
               { email: "auw@i.ua", password: "їїї" },
               { email: "", password: "" },
               { password: "" }
          ];
let someComment = {
     courseId: '',
     text: "at Object.<anonymous> (d:\Roadmap\www\Projects\Binary\pet-api-testing\tempCodeRunnerFile.js:1589:30)"
},
opt = { "size": 200 };

describe('Check using test data', () => {
     let login, userId;
     let listOfCourses = [
    {
        "id": "79b02587-7149-4eae-b319-4f8f8cc7d8b6",
        "name": "vxcvcvcvxc",
        "level": "BEGINNER",
        "authorId": "5aeff799-1066-4956-a992-04f074b97495",
        "authorName": "Rimma Brown",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-25T22:55:43.681+00:00",
        "duration": 758,
        "description": "cvxcvxvxcvcxvc",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "JavaScript"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "64cac8ab-d810-463a-b59b-b18540adacaa",
        "name": "API_API",
        "level": "INTERMEDIATE",
        "authorId": "9347e3a5-7a76-4559-8ce5-f954d8e0de5e",
        "authorName": "Viktoria Riashyna",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-23T12:16:25.303+00:00",
        "duration": 0,
        "description": "description",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "84f0fd33-f645-426b-9258-60f84d8b22e8",
        "name": "Practical Wooden Shoes",
        "level": "BEGINNER",
        "authorId": "643720a3-1ed1-48a9-8776-cdb5a11ffa93",
        "authorName": "Tester Tester",
        "imageSrc": null,
        "releasedDate": "2022-07-22T20:31:58.210+00:00",
        "duration": 7778,
        "description": "Amet debitis a. Aut cumque nemo. Consequuntur rerum molestiae qui voluptatem commodi et vitae.",
        "rating": 5,
        "lectures": 1,
        "tags": [
            "Graphic"
        ],
        "members": 3,
        "ratingCount": 2
    },
    {
        "id": "4367e6d9-9eb5-484e-abba-8675402620f7",
        "name": "Licensed Fresh Ball",
        "level": "BEGINNER",
        "authorId": "643720a3-1ed1-48a9-8776-cdb5a11ffa93",
        "authorName": "Tester Tester",
        "imageSrc": null,
        "releasedDate": "2022-07-22T20:27:41.698+00:00",
        "duration": 7778,
        "description": "Dolorem quis magni doloribus omnis fugit sapiente magnam nihil. Ut qui ut et. Enim omnis reiciendis dolores omnis corporis id deleniti.",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Graphic"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "7b612fe0-b6ed-41dd-a448-f225925822eb",
        "name": "Sleek Steel Shoes",
        "level": "BEGINNER",
        "authorId": "643720a3-1ed1-48a9-8776-cdb5a11ffa93",
        "authorName": "Tester Tester",
        "imageSrc": null,
        "releasedDate": "2022-07-22T20:26:14.213+00:00",
        "duration": 7778,
        "description": "Vitae quis assumenda dolore perferendis quia. Dolores dicta distinctio dignissimos et non eveniet unde. Necessitatibus illo eum ut occaecati ut reiciendis ea. Deleniti est voluptatem debitis eum quas maxime et repellat ipsum.",
        "rating": 5,
        "lectures": 1,
        "tags": [
            "Graphic"
        ],
        "members": 1,
        "ratingCount": 2
    },
    {
        "id": "011143c6-fe72-4b79-b7d7-d326ac4f4942",
        "name": "Handcrafted Soft Cheese",
        "level": "BEGINNER",
        "authorId": "643720a3-1ed1-48a9-8776-cdb5a11ffa93",
        "authorName": "Tester Tester",
        "imageSrc": null,
        "releasedDate": "2022-07-22T20:26:03.096+00:00",
        "duration": 7778,
        "description": "Qui delectus ipsum voluptatem. Et rerum veniam occaecati explicabo. Vel quis consectetur odit. Odit voluptatem qui non ut. Voluptas aut quas.",
        "rating": 4,
        "lectures": 1,
        "tags": [
            "Graphic"
        ],
        "members": 3,
        "ratingCount": 2
    },
    {
        "id": "d319e4e8-6889-476c-aa8e-b6483c4e1730",
        "name": "Unbranded Plastic Shoes",
        "level": "BEGINNER",
        "authorId": "643720a3-1ed1-48a9-8776-cdb5a11ffa93",
        "authorName": "Tester Tester",
        "imageSrc": null,
        "releasedDate": "2022-07-22T20:02:52.322+00:00",
        "duration": 7778,
        "description": "Eveniet quo et ex similique voluptas. Vero qui veniam similique officiis. Assumenda sapiente repudiandae. Et dolorum fuga aut mollitia. Sapiente ad sit quasi aut maxime autem ex.",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Graphic"
        ],
        "members": 2,
        "ratingCount": 0
    },
    {
        "id": "b9d8b606-f3f6-433c-b071-0ad317c7c840",
        "name": "test1999",
        "level": "ADVANCED",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T18:54:28.323+00:00",
        "duration": 59,
        "description": "test1 test test",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Java"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "953b0908-82f5-4874-aa42-a306444f79dd",
        "name": "Course Update",
        "level": "INTERMEDIATE",
        "authorId": "affe2f4b-cb89-4e6b-88a2-f12c4260bc5e",
        "authorName": "Petro Zrada",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T18:16:22.651+00:00",
        "duration": 984,
        "description": "fsaaaaaaaaaaaafsaaaaaaaaa",
        "rating": 4,
        "lectures": 2,
        "tags": [
            "Angular",
            "Ruby"
        ],
        "members": 2,
        "ratingCount": 1
    },
    {
        "id": "cd474348-2000-4a55-87fa-5d9edf892876",
        "name": "test1999",
        "level": "ADVANCED",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T18:24:09.050+00:00",
        "duration": 59,
        "description": "test1 test test",
        "rating": 4,
        "lectures": 1,
        "tags": [
            "Java"
        ],
        "members": 2,
        "ratingCount": 1
    },
    {
        "id": "9ebac1cf-6772-43bb-9a5d-2d63aee2bbfa",
        "name": "New Course",
        "level": "ADVANCED",
        "authorId": "affe2f4b-cb89-4e6b-88a2-f12c4260bc5e",
        "authorName": "Petro Zrada",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T18:13:03.879+00:00",
        "duration": 984,
        "description": "fsaaaaaaaaaaaafsaaaaaaaaa",
        "rating": 0,
        "lectures": 2,
        "tags": [
            "Angular",
            "Ruby"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "7e54b2fb-8991-4b01-ad0b-a345d4cfa2bb",
        "name": "afffffffffffffasf",
        "level": "ADVANCED",
        "authorId": "affe2f4b-cb89-4e6b-88a2-f12c4260bc5e",
        "authorName": "Petro Zrada",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T17:49:47.854+00:00",
        "duration": 984,
        "description": "fsaaaaaaaaaaaafsaaaaaaaaa",
        "rating": 0,
        "lectures": 2,
        "tags": [
            "Angular",
            "Ruby"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "0e05e5f8-bb1b-42ed-ae47-ebef774d4395",
        "name": "QA 1",
        "level": "BEGINNER",
        "authorId": "affe2f4b-cb89-4e6b-88a2-f12c4260bc5e",
        "authorName": "Petro Zrada",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T17:48:45.866+00:00",
        "duration": 984,
        "description": "New bsa QA LECTURE POSTMAN",
        "rating": 0,
        "lectures": 2,
        "tags": [
            "Angular",
            "Ruby"
        ],
        "members": 2,
        "ratingCount": 0
    },
    {
        "id": "d9a180e2-1aa9-44dd-9e3a-efdc4e10e59e",
        "name": "QA",
        "level": "BEGINNER",
        "authorId": "affe2f4b-cb89-4e6b-88a2-f12c4260bc5e",
        "authorName": "Petro Zrada",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T17:41:14.942+00:00",
        "duration": 984,
        "description": "fasfaaaaaaaaaaaaaaasafsafaf",
        "rating": 0,
        "lectures": 2,
        "tags": [
            "Angular",
            "Ruby"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "3f7f7da5-3c5c-482f-916f-a6da8766c8e6",
        "name": "New course",
        "level": "INTERMEDIATE",
        "authorId": "c9409421-d44d-4547-bd1a-6d9d07d461ba",
        "authorName": "AA AA",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T17:35:18.406+00:00",
        "duration": 3600,
        "description": "Course description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "b10ef728-be2e-4661-8782-c4194d4b77b2",
        "name": "New course",
        "level": "INTERMEDIATE",
        "authorId": "c9409421-d44d-4547-bd1a-6d9d07d461ba",
        "authorName": "AA AA",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T17:27:54.597+00:00",
        "duration": 3600,
        "description": "Course description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "48f36418-8816-4e27-aaec-505a9b7744cf",
        "name": "New course",
        "level": "INTERMEDIATE",
        "authorId": "c9409421-d44d-4547-bd1a-6d9d07d461ba",
        "authorName": "AA AA",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T17:17:40.691+00:00",
        "duration": 3600,
        "description": "Course description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "407fdab6-ad19-4ad5-b4a4-23b2f88e1dbc",
        "name": "Tasty Wooden Car",
        "level": "BEGINNER",
        "authorId": "643720a3-1ed1-48a9-8776-cdb5a11ffa93",
        "authorName": "Tester Tester",
        "imageSrc": null,
        "releasedDate": "2022-07-22T17:16:23.468+00:00",
        "duration": 7778,
        "description": "Hic error sint ipsam amet architecto quo quisquam animi. Rerum similique quas voluptatum. Molestias suscipit necessitatibus adipisci illum dolores. Esse facere et quisquam totam blanditiis labore mollitia autem ipsa. Voluptas dolor ea rerum perferendis recusandae voluptas ex enim.",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Graphic"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "10365b96-e5cc-4e12-9e79-b4c60fa044fd",
        "name": "Incredible Soft Bike",
        "level": "BEGINNER",
        "authorId": "643720a3-1ed1-48a9-8776-cdb5a11ffa93",
        "authorName": "Tester Tester",
        "imageSrc": null,
        "releasedDate": "2022-07-22T17:15:29.459+00:00",
        "duration": 7778,
        "description": "In alias est corporis officiis eos aut. Magnam qui blanditiis. Itaque voluptates non in. Eum nostrum esse velit sint sed porro amet et soluta. Ut et neque voluptatem tempora eos et velit est molestiae.",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Graphic"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "2e130428-8e95-49fb-a308-8a8dfc422c9f",
        "name": "New course",
        "level": "INTERMEDIATE",
        "authorId": "c9409421-d44d-4547-bd1a-6d9d07d461ba",
        "authorName": "AA AA",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T16:53:43.187+00:00",
        "duration": 3600,
        "description": "Course description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "6d990b42-308d-41ee-895d-5526f1e78d98",
        "name": "New course",
        "level": "INTERMEDIATE",
        "authorId": "c9409421-d44d-4547-bd1a-6d9d07d461ba",
        "authorName": "AA AA",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T16:52:28.112+00:00",
        "duration": 3600,
        "description": "Course description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "6852c4d2-6839-4647-b976-d42e92599341",
        "name": "New course",
        "level": "INTERMEDIATE",
        "authorId": "c9409421-d44d-4547-bd1a-6d9d07d461ba",
        "authorName": "AA AA",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T16:52:07.185+00:00",
        "duration": 3600,
        "description": "Course description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "8d2d53f5-2599-4068-b82e-61d0e7833ca4",
        "name": "New course",
        "level": "INTERMEDIATE",
        "authorId": "c9409421-d44d-4547-bd1a-6d9d07d461ba",
        "authorName": "AA AA",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T16:51:15.830+00:00",
        "duration": 3600,
        "description": "Course description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "423890ca-109e-42e5-b39c-b9275b82e64d",
        "name": "New course",
        "level": "INTERMEDIATE",
        "authorId": "c9409421-d44d-4547-bd1a-6d9d07d461ba",
        "authorName": "AA AA",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T16:50:18.768+00:00",
        "duration": 3600,
        "description": "Course description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "accafc91-6ff7-4e8f-b4e5-fed0baabf209",
        "name": "Unbranded Fresh Computer",
        "level": "BEGINNER",
        "authorId": "643720a3-1ed1-48a9-8776-cdb5a11ffa93",
        "authorName": "Tester Tester",
        "imageSrc": null,
        "releasedDate": "2022-07-22T16:47:04.663+00:00",
        "duration": 7778,
        "description": "Eaque unde voluptas aperiam nihil. Fuga nemo omnis tenetur rerum alias quo quis odit. Laborum commodi quos distinctio maiores ex. Necessitatibus praesentium in et.",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Graphic"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "43e98ebc-28e4-4e50-8d41-e40c3536c764",
        "name": "Sleek Plastic Fish",
        "level": "BEGINNER",
        "authorId": "643720a3-1ed1-48a9-8776-cdb5a11ffa93",
        "authorName": "Tester Tester",
        "imageSrc": null,
        "releasedDate": "2022-07-22T16:46:18.820+00:00",
        "duration": 7778,
        "description": "Ratione vero totam officiis quo quia dolore. Sit dolor et nesciunt rerum quia ipsam ex omnis quaerat. Tenetur maxime necessitatibus in rerum voluptatem quia. Voluptatum totam quis necessitatibus ea qui deleniti. Iusto magnam maiores commodi omnis.",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Graphic"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "8fe26eb5-180b-4e08-bcaa-a59d30b8e3fb",
        "name": "New course",
        "level": "INTERMEDIATE",
        "authorId": "c9409421-d44d-4547-bd1a-6d9d07d461ba",
        "authorName": "AA AA",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T16:46:10.801+00:00",
        "duration": 3600,
        "description": "Course description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "14f69d69-045a-4fc8-8124-1a65a38ab3e5",
        "name": "New course",
        "level": "INTERMEDIATE",
        "authorId": "c9409421-d44d-4547-bd1a-6d9d07d461ba",
        "authorName": "AA AA",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T16:44:23.549+00:00",
        "duration": 3600,
        "description": "Course description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "9a54d6bc-9c17-408a-8f5c-925d698bf3e8",
        "name": "Anna",
        "level": "INTERMEDIATE",
        "authorId": "03a4a98b-6bb1-42af-a154-90d0dc7428af",
        "authorName": "Anna Hlava",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T16:42:57.937+00:00",
        "duration": 1005,
        "description": "fbgffuyuikjgasddasd",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Android"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "afca98df-3fd9-4f82-aa87-e6f9e62546c6",
        "name": "Panyk course by API",
        "level": "BEGINNER",
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T16:07:53.224+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "426e06d7-5a43-4894-b7d1-211f713cf3a9",
        "name": "Test Post Course Testing",
        "level": "BEGINNER",
        "authorId": "ba3288fd-6330-46e4-b7af-e00819b0e8c3",
        "authorName": "Serhii Popenko",
        "imageSrc": null,
        "releasedDate": "2022-07-22T15:57:58.243+00:00",
        "duration": 0,
        "description": "Test Post Course Testing",
        "rating": 15,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 0,
        "ratingCount": 1
    },
    {
        "id": "e99ef098-31fb-461f-be82-3a5249bbdfbb",
        "name": "Test Post Course  Testing Testing",
        "level": "BEGINNER",
        "authorId": "ba3288fd-6330-46e4-b7af-e00819b0e8c3",
        "authorName": "Serhii Popenko",
        "imageSrc": null,
        "releasedDate": "2022-07-22T15:57:53.250+00:00",
        "duration": 0,
        "description": "Test Post Course Testing",
        "rating": 2,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 0,
        "ratingCount": 1
    },
    {
        "id": "a36c71c2-e3b4-4173-b829-2b69cc12b801",
        "name": "Panyk course by API",
        "level": "BEGINNER",
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T15:54:09.959+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "aeb05db9-373b-4ad3-bd88-7058f21f5686",
        "name": "Panyk course by API",
        "level": "BEGINNER",
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T15:44:55.436+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "a65c7d95-074b-4140-a839-2d84898e75a8",
        "name": "PHP: от 6 до 8",
        "level": "ADVANCED",
        "authorId": "5b311396-ef0c-45ea-909e-77044fe1cba1",
        "authorName": "string string",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T14:48:11.918+00:00",
        "duration": 563,
        "description": "пыпаывпыавпап",
        "rating": 5,
        "lectures": 1,
        "tags": [
            "PHP"
        ],
        "members": 7,
        "ratingCount": 1
    },
    {
        "id": "22490a97-2184-4e03-8306-1ba4587f7bc1",
        "name": "PHP: от 4 до 6",
        "level": "ADVANCED",
        "authorId": "5b311396-ef0c-45ea-909e-77044fe1cba1",
        "authorName": "string string",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T14:47:44.681+00:00",
        "duration": 563,
        "description": "ыкекекпкыпк",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "PHP"
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "ce83f9a7-d4c5-4be6-bdf2-a0eaf5c91092",
        "name": "PHP: от 2 до 4",
        "level": "INTERMEDIATE",
        "authorId": "5b311396-ef0c-45ea-909e-77044fe1cba1",
        "authorName": "string string",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T14:47:10.034+00:00",
        "duration": 563,
        "description": "апа",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "PHP"
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "fdc5a49d-79f3-4657-b6b0-e7c503e453dc",
        "name": "Panyk course by API",
        "level": "BEGINNER",
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T14:30:52.618+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "26ff5088-20ac-457f-9653-da3bbab84a08",
        "name": "Panyk course by API",
        "level": "BEGINNER",
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T14:29:40.145+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "63bfb666-54f0-407f-88b0-5e9051c9d001",
        "name": "Panyk course by API",
        "level": "BEGINNER",
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T14:28:17.394+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 2,
        "ratingCount": 0
    },
    {
        "id": "8dbb86dc-1da8-40f8-b985-94d96b245be4",
        "name": "Panyk course by API",
        "level": "BEGINNER",
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T14:09:37.847+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 4,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 1,
        "ratingCount": 1
    },
    {
        "id": "4f3527b6-13d0-4073-92aa-40755286feb5",
        "name": "UPDATED",
        "level": "BEGINNER",
        "authorId": "7453ce46-9a8d-4706-a8ea-4e2e6dab6331",
        "authorName": "Dayana Abshire",
        "imageSrc": "http://placeimg.com/640/480/animals",
        "releasedDate": "2022-07-22T14:02:12.355+00:00",
        "duration": 212,
        "description": "UPDATED DESC",
        "rating": 4,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 4,
        "ratingCount": 2
    },
    {
        "id": "1a5750c6-e1a9-4465-a5d9-03ede0b3429e",
        "name": "UPDATED",
        "level": "BEGINNER",
        "authorId": "83daa838-ce26-49ab-90a6-e423d7e6976b",
        "authorName": "Mary Huel",
        "imageSrc": "http://placeimg.com/640/480/animals",
        "releasedDate": "2022-07-22T13:52:47.120+00:00",
        "duration": 212,
        "description": "UPDATED DESC",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "cf3dd717-fe68-4011-ac54-29a099ac462d",
        "name": "UPDATED",
        "level": "BEGINNER",
        "authorId": "83daa838-ce26-49ab-90a6-e423d7e6976b",
        "authorName": "Mary Huel",
        "imageSrc": "http://placeimg.com/640/480/animals",
        "releasedDate": "2022-07-22T13:41:21.868+00:00",
        "duration": 212,
        "description": "UPDATED DESC",
        "rating": 5,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 1,
        "ratingCount": 1
    },
    {
        "id": "c3d989bc-4017-44e4-8091-9f0aa62865f6",
        "name": "UPDATED",
        "level": "BEGINNER",
        "authorId": "83daa838-ce26-49ab-90a6-e423d7e6976b",
        "authorName": "Mary Huel",
        "imageSrc": "http://placeimg.com/640/480/animals",
        "releasedDate": "2022-07-22T13:38:12.657+00:00",
        "duration": 212,
        "description": "UPDATED DESC",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "7edf4f2c-829c-4386-a666-cd7d4686f500",
        "name": "Practical Cotton Shirt",
        "level": "BEGINNER",
        "authorId": "83daa838-ce26-49ab-90a6-e423d7e6976b",
        "authorName": "Mary Huel",
        "imageSrc": "http://placeimg.com/640/480/nature",
        "releasedDate": "2022-07-22T13:13:43.271+00:00",
        "duration": 212,
        "description": "Super description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "d5fd8885-ba1c-4d41-86fd-609d4f95ddab",
        "name": "Unbranded Metal Pants",
        "level": "BEGINNER",
        "authorId": "83daa838-ce26-49ab-90a6-e423d7e6976b",
        "authorName": "Mary Huel",
        "imageSrc": "http://placeimg.com/640/480/nature",
        "releasedDate": "2022-07-22T13:13:40.983+00:00",
        "duration": 212,
        "description": "Super description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "e8662738-4fa3-4dc3-82f9-8bda774a3156",
        "name": "Panyk course by API",
        "level": "BEGINNER",
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T12:01:56.725+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "63350002-e530-4be0-8873-0b324855d7cf",
        "name": "UPDATED",
        "level": "BEGINNER",
        "authorId": "4c926f77-ebbf-4361-a595-ec1b11cde3af",
        "authorName": "Tre Ortiz",
        "imageSrc": "http://placeimg.com/640/480/animals",
        "releasedDate": "2022-07-22T10:24:59.666+00:00",
        "duration": 212,
        "description": "UPDATED DESC",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "39a17a6d-a3af-4c30-96fc-f65c52e68d08",
        "name": "Handmade Fresh Cheese",
        "level": "BEGINNER",
        "authorId": "4c926f77-ebbf-4361-a595-ec1b11cde3af",
        "authorName": "Tre Ortiz",
        "imageSrc": "http://placeimg.com/640/480/nature",
        "releasedDate": "2022-07-22T10:08:32.725+00:00",
        "duration": 212,
        "description": "Super description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "8f7171e5-0903-4f87-80b7-529e4a54d412",
        "name": "Awesome Steel Ball",
        "level": "BEGINNER",
        "authorId": "4c926f77-ebbf-4361-a595-ec1b11cde3af",
        "authorName": "Tre Ortiz",
        "imageSrc": "http://placeimg.com/640/480/nature",
        "releasedDate": "2022-07-22T10:08:15.869+00:00",
        "duration": 212,
        "description": "",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "72a7ad96-06f4-44f3-8904-ed9f5f82433b",
        "name": "Practical Steel Cheese",
        "level": "BEGINNER",
        "authorId": "4c926f77-ebbf-4361-a595-ec1b11cde3af",
        "authorName": "Tre Ortiz",
        "imageSrc": "http://placeimg.com/640/480/nature",
        "releasedDate": "2022-07-22T10:07:33.830+00:00",
        "duration": 212,
        "description": "",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "2db00437-ebcc-4ad7-b512-8c58566de9f3",
        "name": "Gorgeous Metal Tuna",
        "level": "BEGINNER",
        "authorId": "4c926f77-ebbf-4361-a595-ec1b11cde3af",
        "authorName": "Tre Ortiz",
        "imageSrc": "http://placeimg.com/640/480/nature",
        "releasedDate": "2022-07-22T10:06:04.882+00:00",
        "duration": 212,
        "description": "",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "144b5e2f-c5dc-49f5-8e4d-65b2ce6cdb04",
        "name": "Refined Frozen Soap",
        "level": "BEGINNER",
        "authorId": "4c926f77-ebbf-4361-a595-ec1b11cde3af",
        "authorName": "Tre Ortiz",
        "imageSrc": "http://placeimg.com/640/480/cats",
        "releasedDate": "2022-07-22T09:30:57.957+00:00",
        "duration": 212,
        "description": "Deleniti quaerat quam velit et non omnis deserunt autem occaecati. Occaecati ut similique repellat rerum dignissimos minus dolorem. Rerum eum aut ratione velit quidem aliquid accusantium voluptas molestias. Fugiat et voluptas consequatur molestias. Nihil quibusdam enim a aut laborum quia. Vel qui rerum illum delectus reiciendis hic quis qui.",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "95a84f15-44da-44b9-997f-4cb90120b87a",
        "name": "wesome Concrete Gloves",
        "level": "BEGINNER",
        "authorId": "4c926f77-ebbf-4361-a595-ec1b11cde3af",
        "authorName": "Tre Ortiz",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T09:21:40.773+00:00",
        "duration": 425,
        "description": "Perferendis assumenda cum omnis aut distinctio. Pariatur omnis exercitationem quidem corrupti nihil rerum maxime unde. Aut quia suscipit error labore et assumenda quod possimus distinctio. Quis voluptas deserunt quae labore est voluptatem commodi asperiores illo. Facere assumenda possimus necessitatibus fugiat sit voluptatem adipisci doloribus recusandae.",
        "rating": 0,
        "lectures": 2,
        "tags": [
            "Swift"
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "d0eab51a-c22f-4ebc-81df-ff31b4e3d457",
        "name": "ww",
        "level": "BEGINNER",
        "authorId": "9e73ad31-7321-44ab-bdcd-898114d8fd8f",
        "authorName": "Inga lew",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T07:48:32.866+00:00",
        "duration": 201,
        "description": "wwwwxxxxxxxxxxxxx",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "PHP"
        ],
        "members": 2,
        "ratingCount": 0
    },
    {
        "id": "47937cef-c235-43c5-aac1-8cf1fc19a403",
        "name": "Test by Panuk",
        "level": "BEGINNER",
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-22T07:43:45.437+00:00",
        "duration": 1333,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "rating": 5,
        "lectures": 1,
        "tags": [
            "Swift"
        ],
        "members": 1,
        "ratingCount": 1
    },
    {
        "id": "9c683276-a770-465b-9b5e-6659313a2bfc",
        "name": "Panyk course by API",
        "level": "BEGINNER",
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T06:26:48.769+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "5f06b282-b897-4fbf-862d-75fa186c3827",
        "name": "Panyk course by API",
        "level": null,
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T06:26:19.071+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "01043734-8ef7-44e9-a155-c8925aca3c9f",
        "name": "Panyk course by API",
        "level": null,
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T06:26:10.839+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "62cc715d-6d98-470b-97bc-2b34bd79500e",
        "name": "Panyk course by API",
        "level": null,
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T06:26:04.343+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "92ef5b68-9eb4-40c0-880c-2bb07943708d",
        "name": "Panyk course by API",
        "level": null,
        "authorId": "b1d58791-7656-471b-b70a-7687bde9dddf",
        "authorName": "Andrii Panyk",
        "imageSrc": "",
        "releasedDate": "2022-07-22T06:25:47.976+00:00",
        "duration": 0,
        "description": "string ffdgfg gerg grg erg erge g",
        "rating": 0,
        "lectures": 0,
        "tags": [
            null
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "229d2f91-f102-423e-a168-cc9315be4d24",
        "name": "test1999",
        "level": "ADVANCED",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T22:26:24.313+00:00",
        "duration": 59,
        "description": "test1 test test",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Java"
        ],
        "members": 2,
        "ratingCount": 0
    },
    {
        "id": "ce3550a0-d065-4d81-b46c-ef1d6fb29eab",
        "name": "test1",
        "level": "ADVANCED",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T21:27:46.235+00:00",
        "duration": 59,
        "description": "test1 test test ",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Java"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "4acb6855-a693-4571-871d-3a9c24a8d4ae",
        "name": "First course",
        "level": "INTERMEDIATE",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T21:27:41.188+00:00",
        "duration": 6,
        "description": "First course",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Angular",
            "C++",
            "Swift"
        ],
        "members": 4,
        "ratingCount": 0
    },
    {
        "id": "90b51007-f849-4890-8adf-654e83616b77",
        "name": "Our test course",
        "level": "BEGINNER",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T19:52:21.112+00:00",
        "duration": 59,
        "description": "test test test",
        "rating": 4,
        "lectures": 1,
        "tags": [
            "Java"
        ],
        "members": 2,
        "ratingCount": 2
    },
    {
        "id": "8786a8c5-e3d1-42ae-8853-c10ac76201cb",
        "name": "test38888",
        "level": "ADVANCED",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T20:41:32.175+00:00",
        "duration": 59,
        "description": "test1 test test",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Java"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "a50e69af-5157-4d80-868c-b08429c88508",
        "name": "test1999",
        "level": "ADVANCED",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T20:03:38.847+00:00",
        "duration": 59,
        "description": "test1 test test",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Java"
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "aa6799bc-6c32-4d4d-886d-e8cc23243cb6",
        "name": "test1",
        "level": "ADVANCED",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T20:01:57.973+00:00",
        "duration": 59,
        "description": "test1 test test",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Java"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "1255d3f0-e870-48b1-9085-cd843e72c033",
        "name": "test333",
        "level": "ADVANCED",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T19:54:43.195+00:00",
        "duration": 59,
        "description": "test1 test test",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Java"
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "e5d787b6-1f74-4052-8251-94a1a966e6b9",
        "name": "The Practical Test Pyramid",
        "level": "BEGINNER",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T19:34:10.168+00:00",
        "duration": 220,
        "description": "The \"Test Pyramid\" is a metaphor that tells us to group software tests into buckets of different granularity. It also gives an idea of how many tests we should have in each of these groups. Although the concept of the Test Pyramid has been around for a while, teams still struggle to put it into practice properly. This article revisits the original concept of the Test Pyramid and shows how you can put this into practice. It shows which kinds of tests you should be looking for in the different levels of the pyramid and gives practical examples on how these can be implemented. ",
        "rating": 4,
        "lectures": 2,
        "tags": [
            "Angular",
            "C#",
            "C++"
        ],
        "members": 0,
        "ratingCount": 1
    },
    {
        "id": "62bbccc9-0f51-46b1-9dfd-9b09b1ba9d9d",
        "name": "adasd as",
        "level": "BEGINNER",
        "authorId": "03a4a98b-6bb1-42af-a154-90d0dc7428af",
        "authorName": "Anna Hlava",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T18:50:12.730+00:00",
        "duration": 3600,
        "description": "ASD ASD SDads",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "Angular",
            "SQL"
        ],
        "members": 2,
        "ratingCount": 0
    },
    {
        "id": "b6ccc214-40ca-4a57-b9f2-334bf5ed690f",
        "name": "vds gsd ",
        "level": "BEGINNER",
        "authorId": "b83221f0-9214-4c80-8778-92c6071da685",
        "authorName": "Anna Hlava",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T17:42:46.021+00:00",
        "duration": 25200,
        "description": "sdf sd fsadfa ",
        "rating": 0,
        "lectures": 7,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 0,
        "ratingCount": 0
    },
    {
        "id": "8460c675-e7c4-4ef6-ba6c-286927087a4a",
        "name": "Course",
        "level": "BEGINNER",
        "authorId": "b83221f0-9214-4c80-8778-92c6071da685",
        "authorName": "Anna Hlava",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-21T16:56:39.522+00:00",
        "duration": 3600,
        "description": "Description",
        "rating": 0,
        "lectures": 1,
        "tags": [
            "AI",
            "Android",
            "Angular"
        ],
        "members": 1,
        "ratingCount": 0
    },
    {
        "id": "584d8a89-f7c0-4bad-a9cc-631b1e3ebb8e",
        "name": "dfdf",
        "level": "INTERMEDIATE",
        "authorId": "dd75a732-449b-459f-bee4-deb246f31387",
        "authorName": "Professor Severus Snape",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-11T17:09:19.914+00:00",
        "duration": 1931,
        "description": "dffdffffffffffffffff",
        "rating": 5,
        "lectures": 3,
        "tags": [
            "C#"
        ],
        "members": 9,
        "ratingCount": 2
    },
    {
        "id": "cf4cf7e7-362e-4246-bd60-87486cc51f48",
        "name": "PHP: от 0 до 2",
        "level": "BEGINNER",
        "authorId": "5b311396-ef0c-45ea-909e-77044fe1cba1",
        "authorName": "string string",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-11T17:08:41.324+00:00",
        "duration": 563,
        "description": "Обучение PHP с 0 до 2 уровня",
        "rating": 5,
        "lectures": 1,
        "tags": [
            "PHP"
        ],
        "members": 10,
        "ratingCount": 6
    },
    {
        "id": "aaa449e4-01e1-49e6-bf54-e8056f58508a",
        "name": "C# for Dummies",
        "level": "BEGINNER",
        "authorId": "dd75a732-449b-459f-bee4-deb246f31387",
        "authorName": "Professor Severus Snape",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-11T16:48:29.709+00:00",
        "duration": 1931,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. \n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "rating": 5,
        "lectures": 3,
        "tags": [
            "C#"
        ],
        "members": 6,
        "ratingCount": 2
    },
    {
        "id": "49606176-54da-4b2b-ad26-dec87b9a7ff3",
        "name": "dfdv",
        "level": "BEGINNER",
        "authorId": "a18a662b-6e74-4791-9500-c9ed5808ae3b",
        "authorName": "Emma fikd",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-11T15:14:23.980+00:00",
        "duration": 23,
        "description": "gffv vgfbg fdv",
        "rating": 4,
        "lectures": 1,
        "tags": [
            "Java"
        ],
        "members": 9,
        "ratingCount": 5
    },
    {
        "id": "2f89aaec-62c2-46ac-9ab2-53b4fa0c3caf",
        "name": "How to build chatbots",
        "level": "INTERMEDIATE",
        "authorId": "002ef363-138c-4504-abbe-c6c905bb8a56",
        "authorName": "Tommatto Senior",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-11T14:00:48.318+00:00",
        "duration": 582,
        "description": "A few lessons about chatbots' building",
        "rating": 4,
        "lectures": 1,
        "tags": [
            "Java",
            "JavaScript",
            "Python"
        ],
        "members": 13,
        "ratingCount": 7
    },
    {
        "id": "beb9cdbd-f036-4073-84cf-6621123e5f50",
        "name": " Солома ігри річка!",
        "level": "ADVANCED",
        "authorId": "fdce8a8c-f786-437b-b6f3-404eb39176a3",
        "authorName": "Captain Obvious",
        "imageSrc": "https://knewless.tk/assets/images/6d96b653-e424-4631-ae16-5f3e8ce1e573.jpg",
        "releasedDate": "2022-07-11T13:08:36.019+00:00",
        "duration": 36601,
        "description": "мпаттапт і ваіваіяваічваівпіпіу2332 ' ' ' ' \n\nіу23к2 2  а '' ' кпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' \nкпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' кпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' \nкпіауафіуца афіуаі уаі Lorem ipsum, dolor sit amet consectetur adipisicing elit. Autem nemo id placeat, provident architecto velit voluptate iusto veniam nihil tempora iure porro saepe quae excepturi error eos odio ipsum? Qui.\nуфа фіу23к2 2  а '' ' \nкпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' \n\nіу23к2 2  а '' ' кпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' \nкпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' кпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' \nкпіауафіуца афіуаі уаі уLorem ipsum, dolor sit amet consectetur adipisicing elit. Autem nemo id placeat, provident architecto velit voluptate iusto veniam nihil tempora iure porro saepe quae excepturi error eos odio ipsum? Qui.\nфа фіу23к2 2  а '' ' \nкпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' \n\nіу23к2 2  а '' ' кпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' \nкпіауафіуца афіуаі уаі уфа Lorem ipsum, dolor sit amet consectetur adipisicing elit. Autem nemo id placeat, provident architecto velit voluptate iusto veniam nihil tempora iure porro saepe quae excepturi error eos odio ipsum? Qui.\nфіу23к2 2  а '' ' кпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' \nкпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' \nкпіауафіуца афіуаі уаі уфа фіу23к2 2  а '' ' ",
        "rating": 5,
        "lectures": 2,
        "tags": [
            "C#",
            "CSS",
            "Design"
        ],
        "members": 23,
        "ratingCount": 10
    },
    {
        "id": "39cfac18-9049-4d09-bee5-bbf30ed81ad0",
        "name": "dsdsa",
        "level": "BEGINNER",
        "authorId": "f65f71a3-9abd-4deb-958a-a70f54823b30",
        "authorName": "Test Test",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-11T10:04:30.448+00:00",
        "duration": 15,
        "description": "dsadasdsadasd",
        "rating": 4,
        "lectures": 1,
        "tags": [
            "Design"
        ],
        "members": 8,
        "ratingCount": 5
    },
    {
        "id": "856e14d5-9fac-42a2-93ea-6805e1974a6e",
        "name": "Java for  cat",
        "level": "BEGINNER",
        "authorId": "75bbe173-8594-4c8a-a926-7a6508b876af",
        "authorName": "Cat Supercat",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-10T19:34:15.600+00:00",
        "duration": 59,
        "description": "Java for  cat",
        "rating": 4,
        "lectures": 1,
        "tags": [
            "Java"
        ],
        "members": 23,
        "ratingCount": 12
    },
    {
        "id": "1db5668e-500e-4237-a0bb-79db980054ac",
        "name": "test course",
        "level": "BEGINNER",
        "authorId": "9e73ad31-7321-44ab-bdcd-898114d8fd8f",
        "authorName": "Inga lew",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-03T08:11:13.353+00:00",
        "duration": 201,
        "description": "this is a course for testing ",
        "rating": 4,
        "lectures": 1,
        "tags": [
            "PHP"
        ],
        "members": 32,
        "ratingCount": 17
    },
    {
        "id": "3f1f8f6e-1bbd-4a6b-9e4b-cac979a3c4b4",
        "name": "JS",
        "level": "INTERMEDIATE",
        "authorId": "9e73ad31-7321-44ab-bdcd-898114d8fd8f",
        "authorName": "Inga lew",
        "imageSrc": "/static/media/default_course_image.3944bf6b.jpg",
        "releasedDate": "2022-07-10T18:17:19.516+00:00",
        "duration": 200,
        "description": "Test course",
        "rating": 4,
        "lectures": 1,
        "tags": [
            "JavaScript"
        ],
        "members": 16,
        "ratingCount": 9
    },
    {
        "id": "0a0ae8aa-8166-4138-a938-438c65f40f42",
        "name": "Python Advanced",
        "level": "ADVANCED",
        "authorId": "6316fb2a-97a9-46e1-990d-11e366444839",
        "authorName": "Test Author",
        "imageSrc": "https://knewless.tk/assets/images/7c7e5515-835e-43a7-9236-1c809a3661c9.jpg",
        "releasedDate": "2022-07-01T19:43:08.470+00:00",
        "duration": 611,
        "description": "Some description",
        "rating": 4,
        "lectures": 2,
        "tags": [
            "JavaScript",
            "Python"
        ],
        "members": 39,
        "ratingCount": 28
    },
    {
        "id": "67b9cef5-0c8c-4728-9246-a3973c18f6f1",
        "name": "Another test course",
        "level": "BEGINNER",
        "authorId": "6316fb2a-97a9-46e1-990d-11e366444839",
        "authorName": "Test Author",
        "imageSrc": "https://knewless.tk/assets/images/fcc7fe9a-a344-4ae2-8f04-2fa404286d3f.jpg",
        "releasedDate": "2022-07-10T09:47:25.457+00:00",
        "duration": 15,
        "description": "Test description",
        "rating": 4,
        "lectures": 1,
        "tags": [
            "Python"
        ],
        "members": 29,
        "ratingCount": 22
    }
]
     before("Logs in with valid credentials", async () => {
          login = await AddToken.login(credentials);
          let response = await login.student.getStudentInfo();
          userId = response.body.userId;
          statusCode200(response);
          respTimeVerification(response, 500);
     });

     invalidCredentialsDataSet.forEach((cred) => {
          it(`Check signing up with invalid credential`, async () => {
               let response = await (new Register()).signUp(cred);
               respTimeVerification(response, 500);
               statusCodeErrorVerification(response);
          });
     })

     let listOfCoursesId = listOfCourses.map(item => item.id);
     listOfCoursesId.forEach((courseId, i) => {
          it(`${i}. Student left comment on course ${courseId}. Total student comments increased by one`, async () => {
               someComment.courseId = courseId;
               let responseBeforeChanges = (await login.commentOnCours.getCourseCommentById(courseId, opt)).body;
               let myCommentsBeforeChanges = responseBeforeChanges.filter(item => (<any>item.user).id === userId).length;
               
               let response = await login.commentOnCours.postCommentOnCourse(someComment);
               statusCode200(response);
               respTimeVerification(response, 500);
               
               let responseAfterChanges = (await login.commentOnCours.getCourseCommentById(courseId, opt)).body;
               let myCommentsAfterChanges = responseAfterChanges.filter(item => (<any>item.user).id === userId).length;
               expect(myCommentsAfterChanges, `The total quantity of your comments should have increased by one`).to.be.equal(myCommentsBeforeChanges+1);
          });
     });
});
