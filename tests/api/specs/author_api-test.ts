import {expect } from "chai";
import { afterEach } from "mocha";
import { getRandomArticleId, respTimeVerification, schemaVerification, statusCode200, statusCodeVerification } from "../../helpers/functionForTests.helper";
import { AddToken } from "../lib/controllers/token.controller";

const schemas = require('./data/schemas_testData.json');
// var chai = require('chai');
// chai.use(require('chai-json-schema'));

const authorInfo = {
          "firstName": "Anya",
          "lastName": "Smith",
          "job": "District Creative Assistant",
          "company": "Gibson, Kiehn and Thiel",
          "biography": "If we transmit the system, we can get to the HDD sensor through the redundant SCSI program!"
     },
     authorInfoNew: {
          avatar?: string | undefined;
          biography?: string | undefined;
          company?: string | undefined;
          firstName: string;
          id: string;
          job?: string | undefined;
          lastName: string;
          location: string;
          twitter?: string | undefined;
          userId?: string | undefined;
          website?: string | undefined;
          } = {
          "id": "",
          "userId": "",
          "avatar": "http://placeimg.com/640/480/cats",
          "firstName": "wwwww",
          "lastName": "wwwww",
          "job": "wwwww",
          "location": "Afghanistan",
          "company": "wwww",
          "website": "https://chadrick.net",
          "twitter": "",
          "biography": "www"
     },
     someArticle = 
          {
          "authorId": "",
          "authorName": "",
          "id": "",
          "image": "https://www.meme-arsenal.com/memes/01407ddd1273ccc7bca1659d77d8256c.jpg",
          "name": "Lorem ipsum",
          "text": "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Odit maiores, delectus doloribus provident quas, blanditiis necessitatibus iure, ipsum dolor pariatur magnam consequuntur? Nesciunt reprehenderit delectus fugiat saepe dolores ab voluptatum."
     },
     someComment: {articleId: string | undefined;
                    text: string | undefined;
                    } = {
          "articleId": "",
          "text": "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Exercitationem inventore labore, iure animi harum quod. Esse, iste cumque quasi distinctio consequuntur dolorum ipsam, dolorem deserunt repellat suscipit laudantium, illo rem."
     },
     opt = { "size": 200 },
     credentials = global.appConfig.users.authorDude1;
     

describe("Knewless controllers ",  () => {
     let corrAmoutOfArticles, login;
   
     before("should give the user the ability to log in", async () => {
          login = await AddToken.login(credentials);
          let response = await login.author.getAuthorInfo();
          statusCode200(response);
          respTimeVerification(response, 500);
          let res = await login.author.getAuthorInfo();
          authorInfoNew.id = res.body.id;
          authorInfoNew.userId = res.body.userId;
     });

    it(`Test 1: get author (GET /author)`, async () => {
          let response = await login.author.getAuthorInfo();
          statusCode200(response);
          respTimeVerification(response, 500);
          schemaVerification(response, schemas.authorGet_schema);
    });
     
     it(`Test 2: change author info (POST author/)`, async () => {
          let response = await login.author.changeAuthorInfo(authorInfoNew);
          let responseNew = await login.author.getAuthorInfo();

          statusCode200(response);
          respTimeVerification(response, 500);
          expect(responseNew.body, `The profile data has been shanged`).to.be.eql(authorInfoNew);
          await login.author.changeAuthorInfo({...authorInfoNew, ...authorInfo});
     });
     
     it("Test 3: get userInfo for profile (/user/me)", async () => {
          let response = await login.user.getUserInfo();

          statusCode200(response);
          respTimeVerification(response, 500);
          schemaVerification(response, schemas.getUserInfo_schema);
     });

     it("Test 4: get author overview (Courses, topics, articles) (/author/overview/{userId})", async () => {
          let response = await login.author.getAuthorOverview(authorInfoNew.id);

          statusCodeVerification(response, 200);
          respTimeVerification(response, 500);
          schemaVerification(response, schemas.getAuthorOverview_schema);
     });
     
      it("Test 5: post author article (POST /article)", async () => {
          let authorOverview = await login.author.getAuthorOverview(authorInfoNew.id);
          let response = await login.article.postArticle(someArticle);
           
          statusCode200(response);
          respTimeVerification(response, 500);
           
          corrAmoutOfArticles = (await login.author.getAuthorOverview(authorInfoNew.id)).body.articles.length;
          expect((corrAmoutOfArticles - authorOverview.body.articles.length), `The total quantity of articles should have increased by one`).to.be.equal(1);
          expect((await login.article.getArticleById(response.body.id)).statusCode, "New article should have been added to DB").to.be.equal(200);
          schemaVerification(response, schemas.authorPostArticle_schema);
      });
     
      it("Test 6: get list of own articles (/article/author)", async () => {
          let response = await login.article.getListOfArticles();
          
          statusCode200(response);
          respTimeVerification(response, 500);
          expect(corrAmoutOfArticles, `Total quantity of articles from different responses must be the same`).to.be.equal(response.body.length);
          schemaVerification(response, schemas.listOfAuthorArticles_schema);
          
      });
     
     it("Test 7: get article by id (/article/{atricleId})", async () => {
          let articleId: any = await getRandomArticleId(login);
          let response = await login.article.getArticleById(articleId);

          statusCode200(response);
          respTimeVerification(response, 500);
          schemaVerification(response, schemas.getArticleById_schema);

      });
     
     it("Test 8: leave comments on articles (POST /article_comment)", async () => {
          // обираємо довільну статтю
          let articleId: any = await getRandomArticleId(login);
          someComment.articleId = articleId;

          //  знаходимо кількість наших власних коментів до неї (по факту ми шукаємо серед 200 останніх по умовам завдання: по чесноку їх потрібно виводити усі, а потім серед них перебирати свої. Але ми знаємо, що їх менше тому залишаю 200 у параметрах пошуку)
          let responseBeforeChanges = await login.commentOnArt.getAllCommentsOnArticle(articleId, opt);
          let myCommentsBeforeChanges = responseBeforeChanges.body.filter(art => (<any>art.user).id === authorInfoNew.userId).length;
          let response = await login.commentOnArt.postCommentOnArticle(someComment);
          statusCode200(response);
          respTimeVerification(response, 500);
          
          //  Перевіряємо, чи додався наш останній комент до попередньої купи))
          let responseAfterChanges = await login.commentOnArt.getAllCommentsOnArticle(articleId, opt);
          let myCommentsAfterChanges = responseAfterChanges.body.filter(art => (<any>art.user).id === authorInfoNew.userId).length;
          expect(myCommentsAfterChanges, `The total quantity of your comments should have increased by one`).to.be.equal(myCommentsBeforeChanges+1);
          schemaVerification(response, schemas.postCommentOnArticle_schema);
     });
     
     it("Test 9: get comment on some article (/article_comment/of/{atricleId}?size=200)", async () => {
          // обираємо довільну статтю
          let articleId: any = await getRandomArticleId(login);
          let response = await login.commentOnArt.getAllCommentsOnArticle(articleId, opt);

          statusCode200(response);
          respTimeVerification(response, 500);
          schemaVerification(response, schemas.getAllCommentsOnArticle_schema);
     });

     afterEach(() => {
          console.log('Awesome text line');
     });
});
